package Talismans2.module.botania.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import vazkii.botania.api.mana.IManaUsingItem;
import vazkii.botania.api.mana.ManaItemHandler;
import baubles.api.BaubleType;

public class ItemManaLightTalisman extends ItemManaTalisman implements IManaUsingItem{
	
	public ItemManaLightTalisman()
	{
		super();
		this.setMaxStackSize(1);
		this.setUnlocalizedName("mana_talisman_light");
	}
	
	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("talismans2:Light_Mana");
	}
	
	
	@Override
	public void onWornTick(ItemStack stack, EntityLivingBase player) 
	{
		super.onWornTick(stack, player);

		if(player instanceof EntityPlayer && !player.worldObj.isRemote) 
		{
			int manaCost = 5;
			boolean hasMana = ManaItemHandler.requestManaExact(stack, (EntityPlayer) player, manaCost, false);
			if(!hasMana)
				onUnequipped(stack, player);
			else {
				if(player.getActivePotionEffect(Potion.nightVision) != null)
					player.removePotionEffect(Potion.nightVision.id);

				player.addPotionEffect(new PotionEffect(Potion.nightVision.id, Integer.MAX_VALUE, 1, true));
			}

			if(player.swingProgress == 0.25F)
				ManaItemHandler.requestManaExact(stack, (EntityPlayer) player, manaCost, true);
		}
	}

	@Override
	public void onUnequipped(ItemStack stack, EntityLivingBase player) 
	{
		PotionEffect effect = player.getActivePotionEffect(Potion.nightVision);
		if(effect != null && effect.getAmplifier() == 1)
			player.removePotionEffect(Potion.nightVision.id);
	}

	@Override
	public BaubleType getBaubleType(ItemStack arg0) 
	{
		return BaubleType.AMULET;
	}

	@Override
	public boolean usesMana(ItemStack stack) 
	{
		return true;
	}

}
