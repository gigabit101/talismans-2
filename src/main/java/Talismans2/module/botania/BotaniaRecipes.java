package Talismans2.module.botania;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipeManaInfusion;
import vazkii.botania.api.recipe.RecipeRuneAltar;
import Talismans2.init.ModItems;

/**
 * @author Gigabit101
 */

public class BotaniaRecipes {

	public static RecipeManaInfusion poolRecipe;
	public static RecipeManaInfusion blankTalisman;
	public static RecipeRuneAltar flameTalisman;
	public static RecipeRuneAltar waterTalisman;
	public static RecipeRuneAltar lightTalisman;
	public static RecipeRuneAltar miningTalisman;
	public static RecipeRuneAltar movementTalisman;

	public static void init()
	{

		blankTalisman = BotaniaAPI.registerManaInfusionRecipe(new ItemStack(
				ModItems.BlankTalisman, 1), new ItemStack(Blocks.stone), 6000);
		// Talismans
		flameTalisman = BotaniaAPI.registerRuneAltarRecipe(new ItemStack(
				BotaniaItems.FlameManaTalisman, 1), 15000, new ItemStack(
				ModItems.BlankTalisman), new ItemStack(Items.gunpowder),
				new ItemStack(Items.flint_and_steel), new ItemStack(
						vazkii.botania.common.item.ModItems.rune,1,1));
		
		waterTalisman = BotaniaAPI.registerRuneAltarRecipe(new ItemStack(
				BotaniaItems.WaterManaTalisman, 1), 15000, new ItemStack(
				ModItems.BlankTalisman), new ItemStack(Blocks.waterlily),
				new ItemStack(Items.water_bucket), new ItemStack(vazkii.botania.common.item.ModItems.rune,1,0));
		
		lightTalisman = BotaniaAPI.registerRuneAltarRecipe(new ItemStack(
				BotaniaItems.LightManaTalisman, 1), 15000, new ItemStack(
				ModItems.BlankTalisman), new ItemStack(Blocks.glowstone),
				new ItemStack(Blocks.redstone_lamp),
				new ItemStack(Blocks.torch),
				new ItemStack(vazkii.botania.common.item.ModItems.rune,1,3));
		
		miningTalisman = BotaniaAPI.registerRuneAltarRecipe(new ItemStack(
				BotaniaItems.MiningManaTalisman, 1), 15000, new ItemStack(
				ModItems.BlankTalisman), new ItemStack(Items.diamond_pickaxe),
				new ItemStack(Items.iron_shovel), new ItemStack(
						Items.nether_wart),
						new ItemStack(vazkii.botania.common.item.ModItems.rune,1,10));
		
		movementTalisman = BotaniaAPI.registerRuneAltarRecipe(new ItemStack(
				BotaniaItems.MovementManaTalisman, 1), 15000, new ItemStack(
				ModItems.BlankTalisman), new ItemStack(Items.diamond_boots),
				new ItemStack(Items.sugar), new ItemStack(Items.golden_apple),
				new ItemStack(vazkii.botania.common.item.ModItems.rune,1,9));

	}

}
