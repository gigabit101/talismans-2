package Talismans2.module.botania;

import cpw.mods.fml.common.registry.GameRegistry;
import Talismans2.module.botania.items.ItemManaFlameTalisman;
import Talismans2.module.botania.items.ItemManaInvisibleTalisman;
import Talismans2.module.botania.items.ItemManaLightTalisman;
import Talismans2.module.botania.items.ItemManaMiningTalisman;
import Talismans2.module.botania.items.ItemManaMovementTalisman;
import Talismans2.module.botania.items.ItemManaWaterTalisman;
import net.minecraft.item.Item;

public class BotaniaItems {
	
	public static Item FlameManaTalisman;
	public static Item WaterManaTalisman;
	public static Item InvisableManaTalisman;
	public static Item MiningManaTalisman;
	public static Item MovementManaTalisman;
	public static Item LightManaTalisman;
	
	public static void init()
	{
		FlameManaTalisman = new ItemManaFlameTalisman();
		GameRegistry.registerItem(FlameManaTalisman, "mana_talisman_flame");
		
		WaterManaTalisman = new ItemManaWaterTalisman();
		GameRegistry.registerItem(WaterManaTalisman, "mana_talisman_water");
		
		InvisableManaTalisman = new ItemManaInvisibleTalisman();
		GameRegistry.registerItem(InvisableManaTalisman, "mana_talisman_invisible");
		
		MiningManaTalisman = new ItemManaMiningTalisman();
		GameRegistry.registerItem(MiningManaTalisman, "mana_talisman_mining");
		
		MovementManaTalisman = new ItemManaMovementTalisman();
		GameRegistry.registerItem(MovementManaTalisman, "mana_talisman_movement");
		
		LightManaTalisman = new ItemManaLightTalisman();
		GameRegistry.registerItem(LightManaTalisman, "mana_talisman_light");
	}

}
