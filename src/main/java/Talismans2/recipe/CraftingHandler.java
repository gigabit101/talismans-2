package Talismans2.recipe;

import net.minecraft.item.ItemStack;
import Talismans2.config.ConfigTalismans;
import Talismans2.init.ModItems;
import Talismans2.util.TalismanStacks;
import cpw.mods.fml.common.registry.GameRegistry;

public class CraftingHandler {

	public static void CopyTalismans(ConfigTalismans properties)
	{
		if (properties.CopyTalismansTrue)
			addCopyRecipes();
			addRingRecipes();
	}

	public static void addCopyRecipes()
	{
		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.FlameTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanFlame);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.WaterTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanWater);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.CraftingTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanCrafting);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.InvisibleTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanInvisible);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.LightTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanLight);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.MiningTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanMining);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.MovementTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanMovement);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.WitherlessTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanWitherless);

		GameRegistry.addShapelessRecipe(new ItemStack (ModItems.MagnetTalisman,2),
				TalismanStacks.talismanBlank, TalismanStacks.talismanMagnet);
	}
	
	public static void addRingRecipes()
	{
		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingFlame,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanFlame);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingWater,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanWater);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingCrafting,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanCrafting);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingInvisible,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanInvisible);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingLight,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanLight);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingMining,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanMining);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingMovement,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanMovement);

		GameRegistry.addShapelessRecipe(TalismanStacks.talismanRingWitherless,
				TalismanStacks.talismanRingBlank, TalismanStacks.talismanWitherless);
	}
}
