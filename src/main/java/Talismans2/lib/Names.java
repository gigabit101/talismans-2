package Talismans2.lib;

/**
 * @author Gigabit101
 */

public class Names {

	public static final class Keys {
		public static final String CATEGORY = "keys.Talismans.category";
		public static final String CRAFT = "keys.Talismans.craft";
	}

}
