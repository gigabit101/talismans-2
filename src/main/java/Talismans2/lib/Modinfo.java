package Talismans2.lib;

/**
 * @author Gigabit101
 */

public class Modinfo 
{

	// Mod ID
	public static final String ID = "Talismans 2";
	// Mod Name
	public static final String NAME = "Talismans 2";
	// Mod Version
	public static final String Version = "1.1.6";
	// Common Proxy
	public static final String SERVER_PROXY_CLASS = "Talismans2.proxies.CommonProxy";
	// Client Proxy
	public static final String CLIENT_PROXY_CLASS = "Talismans2.proxies.ClientProxy";

}
