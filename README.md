[![Build Status](https://drone.io/bitbucket.org/gigabit101/talismans-2/status.png)](https://drone.io/bitbucket.org/gigabit101/talismans-2/latest)
=========

The offical repository of Talismans 2.



Building
=========

We use ForgeGradle. There are instructions inside the 'build.gradle' file in this repository.


Licence
=========

The licence is located at COPYING.txt, but to save you the trouble, it's GPLv3 (with one of the sounds under CC Attribution License). 

Basically, do whatever you want, as long as it's free, open source, and stays under GPL. (Yes, that allows you to use it in your modpack.)

